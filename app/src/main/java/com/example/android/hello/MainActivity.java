package com.filmsbykris.syscmd;

import java.util.*;
import java.net.*;
import java.io.*;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class MainActivity extends Activity {

  TextView tv;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    tv=(TextView)findViewById(R.id.cmdOp);
    Thread t=new Thread(){
      int count = 0;

      @Override
      public void run(){

        while(!isInterrupted()){

          try {

            runOnUiThread(new Runnable() {

              @Override
              public void run() {
                count++;
                tv.setText("Count: ");
                tv.append(String.valueOf(count));
                tv.append("\n");
                tv.append(shell_exec("date"));

              }
            });

            Thread.sleep(1000);  //1000ms = 1 sec
          } catch (InterruptedException e) {
            e.printStackTrace();
          }

        }
      }
    };

    t.start();
  }


  public String shell_exec(String cmd){
    String o="";
    try{
      Process p=Runtime.getRuntime().exec(cmd);
      BufferedReader b=new BufferedReader(new InputStreamReader(p.getInputStream()));
      String line;
      while((line=b.readLine())!=null)o+=line;
    }catch(Exception e){o="error";}
    return o;
  }

}
